FROM ubuntu:22.04
# inspired by rabits/qt which we use for the gcc toolkit

ARG QT_VERSION=6.6.2
ARG QT_TAG=v6.6.2
ARG NDK_VERSION=25.1.8937393
ARG SDK_PLATFORM=android-33
ARG SDK_BUILD_TOOLS=33.0.0
ARG MIN_NDK_PLATFORM=android-21
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV QT_PATH /opt/Qt
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-sdk/ndk/${NDK_VERSION}
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${MIN_NDK_PLATFORM}
ENV ANDROID_API_VERSION ${SDK_PLATFORM}
ENV ANDROID_BUILD_TOOLS_REVISION ${SDK_BUILD_TOOLS}
ENV PATH ${QT_PATH}/bin:${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}
ENV ANDROID_NDK $ANDROID_NDK_ROOT

ENV STANDALONE_EXTRA="--stl=libc++"

RUN apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    # Basic setup dependencies
    ## unzip - unpack source code archives
    unzip \
    ## git - manage git repositoroes
    git \
    ## locales - useful to set utf-8
    locales \
    ## sudo - sudo usage
    sudo \
    ## curl etc. - download source code archives
    curl \
    openssh-client \
    ca-certificates \
    # Basic build dependencies
    ## make, openjdk-11-jdk - basic build requirements
    make \
    openjdk-11-jdk \
    build-essential \
    ninja-build \
    # Other build dependencies
    # (Craft at this time is only used to build (runtime) dependencies for the target platform)
    gettext \
    ## rdfind - used to reduce the size of Android NDK
    rdfind \
    ## gperf - needed to build KCodecs
    gperf \
    ## bison, flex - needed to build KOSMIndoorMap
    bison \
    flex \
    # liblame/ffmpeg/mpv
    nasm \
    # The android tooling also consists of some python scrips, namely generate-fastlane-metadata.py
    # Install the needed python packages for that
    python3 \
    python3-distutils \
    python3-future \
    python3-xdg \
    python3-requests \
    python3-yaml \
    python3-lxml \
    # Craft dependencies
    p7zip-full \
    zstd \
    python3-pip \
    python3-venv \
    # Craft cache upload; client-server communication of ci-notary-service
    python3-paramiko \
    # There are some (build) dependencies of Craft blueprints that don't (cross) compile for Android, but on the host.
    # Craft at this point isn't set up to produce both host and target binaries, so we need host tools in the image.
    ## autoconf, automak and libtool - required to build several things
    autoconf automake libtool \
    ## TODO: STILL NEEDED?
    # autoconf-archive \
    ## autopoint - required to build texinfo
    autopoint \
    ## pkg-config - required to build several things (QtBase among others)
    pkg-config \
    # Finally configure and cleanup things
    && apt-get -qq clean \
    && locale-gen en_US.UTF-8 && dpkg-reconfigure locales \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10

# Install components needed by Gitlab CI
RUN pip install python-gitlab packaging

##########################

RUN chmod a+w /opt/

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

RUN mkdir /output
RUN chown user:user /output

USER user
WORKDIR /home/user
ENV HOME /home/user
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

##########################

# Download & unpack Android SDK
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip' \
    && mkdir -p /opt/android-sdk/cmdline-tools \
    && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk/cmdline-tools \
    && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses \
    && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" "ndk;${NDK_VERSION}" ${SDK_PACKAGES} \
    && sdkmanager --uninstall --verbose emulator

# Compile Native Tooling
RUN mkdir /opt/nativetooling

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://github.com/Kitware/CMake/releases/download/v3.28.3/cmake-3.28.3-linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

RUN cd && git clone https://invent.kde.org/qt/qt/qtbase.git --single-branch --branch ${QT_TAG} \
    && cd qtbase \
    && ./configure -prefix /opt/nativetooling -opensource -confirm-license -no-opengl -release -optimize-size -nomake tests -nomake examples -no-feature-concurrent \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtbase

## QtShaderTools is needed on the host to cross-compile QtShaderTools for the target platform
RUN cd && git clone https://invent.kde.org/qt/qt/qtshadertools.git --single-branch --branch ${QT_TAG} \
    && cd qtshadertools \
    && cmake . -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH=/opt/nativetooling \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtshadertools

## QtQml Needed for native tooling (ki18n)
RUN cd && git clone https://invent.kde.org/qt/qt/qtdeclarative.git --single-branch --branch ${QT_TAG} \
    && cd qtdeclarative \
    && cmake . -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH=/opt/nativetooling \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtdeclarative

## QtTools is needed on the host to cross-compile QtTools for the target platform
RUN cd && git clone https://invent.kde.org/qt/qt/qttools.git --recurse-submodules --single-branch --branch ${QT_TAG} \
    && cd qttools \
    && cmake . -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH=/opt/nativetooling -DFEATURE_assistant=ON \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qttools

## QtRemoteObjects is needed on the host to cross-compile QtRemoteObjects for the target platform
RUN cd && git clone https://invent.kde.org/qt/qt/qtremoteobjects.git --single-branch --branch ${QT_TAG} \
    && cd qtremoteobjects \
    && cmake . -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH=/opt/nativetooling \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtremoteobjects

## must be set after the Qt 6 host build, otherwise configure fails there
ENV QT_HOST_PATH /opt/nativetooling

##########################

ENV PERSIST=0
ARG KDE_CMAKE_HOST_ARGS="-DBUILD_WITH_QT6=ON"

## extra-cmake-modules we can use the KF6 version for KF5-Qt6 and KF6 frameworks
RUN git clone https://invent.kde.org/frameworks/extra-cmake-modules \
    && cmake -B ecm-build -S extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_WITH_QT6=ON \
    && cmake --build ecm-build --target install \
    && rm -r extra-cmake-modules && rm -r ecm-build

## compile kf6 tooling
RUN git clone https://invent.kde.org/frameworks/kconfig \
    && rm -rf kconfig/poqm \
    && cmake -B kconfig-build -S kconfig -DCMAKE_INSTALL_PREFIX=/opt/nativetooling6 -DCMAKE_PREFIX_PATH=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DKCONFIG_USE_DBUS=OFF -DKCONFIG_USE_GUI=OFF ${KDE_CMAKE_HOST_ARGS} \
    && cmake --build kconfig-build -t install \
    && rm -r kconfig && rm -r kconfig-build

RUN git clone https://invent.kde.org/frameworks/kcoreaddons \
    && rm -rf kcoreaddons/poqm \
    && cmake -B kcoreaddons-build -S kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling6 -DCMAKE_PREFIX_PATH=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF ${KDE_CMAKE_HOST_ARGS} \
    && cmake --build kcoreaddons-build -t install \
    && rm -r kcoreaddons && rm -r kcoreaddons-build

# needs to be after building qt, otherwise it breaks weirdly
ENV QMAKESPEC android-clang
ENV PERSIST=1
